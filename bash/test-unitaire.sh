#!/bin/bash

if [ -z "$1" ]
then
echo "need URL in parameter"
else


oldcount=$(curl $1/count/ | jq '.pingCount')
count=$((oldcount+3))

echo 'Envoi de trois requête ping sur '$1''

curl $1/ping/ | jq '.message'
curl $1/ping/ | jq '.message'
curl $1/ping/ | jq '.message'

countAfter=$(curl $1/count/ | jq '.pingCount')


if [ "count"=="countAfter" ]
	then 
		echo 'Le test a fonctionné, au départ il y avait : '$oldcount' au compteur, puis maitenant : '$countAfter' au compteur '

	else
		echo 'Le test n a pas fonctionné '
fi

fi
